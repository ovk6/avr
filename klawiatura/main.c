#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/io.h>

const uint8_t num[] = {
	0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 0x82, 0xf8, 0x80, 0x90
};

inline 
void init()
{
	DDRD |= 0xff;

	TCCR1A = 0;
	TCCR1B = 0;

	OCR1A = 15624;

	TCCR1B |= (1 << WGM12);
	TCCR1B |= (1 << CS00);
	TCCR1B |= (1 << CS01);

	TIMSK |= (1 << OCIE1A);
	
    sei();
}

volatile uint8_t ssd1,ssd2,ssd3,ssd4; 

volatile uint8_t row;
volatile uint8_t col;

inline 
void check_col()
{
	DDRB  = 0x0f;
	PORTB = 0xf0;

	volatile int i;	

	for (i=0; i<15; ++i);

	col = (PINB >> 4);
}

inline 
void check_row()
{
	DDRB  = 0xf0;
	PORTB = 0x0f;

	volatile int i;	

	for (i=0; i<15; ++i);

	row = ~(PINB|0xf0);
}

inline 
void display_num(uint8_t num)
{
   ssd4 = num[num % 10];
   ssd3 = num[(num / 10) % 10];
   ssd2 = num[(num / 100) % 10];
   ssd1 = num[(num / 1000) % 10];
}

inline 
uint8_t high_pin_num(uint8_t reg)
{
	switch (reg) {
		case 0x00:
			return 0;
		case 0x01:
			return 1;
		case 0x02:
			return 2;	
		case 0x04:
			return 3;
		case 0x08:
			return 4;
	}
}

inline 
void display_digit(uint8_t pin, uint8_t num)
{
	DDRC = _BV(pin);
	PORTD = num;

	_delay_ms(2);
}

inline 
void display(uint8_t ssd1, uint8_t ssd2, uint8_t ssd3, uint8_t ssd4)
{
	display_digit(PC1,ssd1);
	display_digit(PC2,ssd2);
	display_digit(PC3,ssd3);
	display_digit(PC4,ssd4);
}

ISR(TIMER1_COMPA_vect)
{
	check_row();
	check_col();

	if (col != 0 && row != 0)
		display_num(high_pin_num(col) + 4 * (high_pin_num(row) - 1));
	else
		display_num(0);

	col = 0;
	row = 0;
}

void main()
{
	init();

    ssd1 = num[0];
    ssd2 = num[0];
    ssd3 = num[0];
    ssd4 = num[0];

	for (;;)
		display(ssd1,ssd2,ssd3,ssd4);
}
