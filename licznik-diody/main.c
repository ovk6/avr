#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/io.h>

inline 
void init()
{
    // Init interrupts
	MCUCR   |= (1 << ISC01) | (0 << ISC00);
	GICR    |= (1 << INT0);

    // Init GPIO
	DDRA    |= 0xff;
  	PORTD   |= _BV(PD1);
	PORTA   = 0x00;

	sei();
}

volatile uint8_t count = 0;

ISR(INT0_vect)
{
	count++;
	PORTA = count;
}

void main()
{
	init();

	for (;;) {
		PORTA ^= _BV(PA7);
		_delay_ms(150);
	}
}
