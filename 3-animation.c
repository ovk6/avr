#include <avr/io.h>
#include <util/delay.h>


int main() {
    DDRC |= _BV(PC0) | _BV(PC1) | _BV(PC2) | _BV(PC3) | _BV(PC4) | _BV(PC5);
    
    volatile int i;

    for(;;) {
        
        for (i = 5; i >= 0; --i) {
            PORTC |= _BV(i);
            _delay_ms(100);
            PORTC &= ~(_BV(i));
        }
        for (i = 0; i != 6; ++i) {
            PORTC |= _BV(i);
            _delay_ms(100);
            PORTC &= ~(_BV(i));
        }
    }
}
