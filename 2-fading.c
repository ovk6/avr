#include <avr/io.h>
#include <util/delay.h>

int main() {
    // set port as output
    DDRC |= _BV(PC0);

    // LED is off initially
    PORTC &= ~(_BV(PC0));
    
    volatile int i;
    volatile int j;
    for(;;) {
        
        for (i = 0; i < 255; i += 2) {
            PORTC |= _BV(PC0);
            for (j=0; j < i; j++);
            PORTC &= ~(_BV(PC0));
            _delay_ms(2);
        }

        for (i = 255; i >= 0; i -= 2) {
            PORTC |= _BV(PC0);
            for (j=0; j < i; j++);
            PORTC &= ~(_BV(PC0));
            _delay_ms(2);
        }
        _delay_ms(100);
    }
}
