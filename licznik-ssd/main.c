#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/io.h>

const uint8_t num[] = {
	0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 0x82, 0xf8, 0x80, 0x90
};

volatile int count;

inline 
void init()
{
	DDRD |= 0xff;

	TCCR1A = 0;
	TCCR1B = 0;

	OCR1A = 15624;

	TCCR1B |= (1 << WGM12);
	TCCR1B |= (1 << CS00);
	TCCR1B |= (1 << CS01);

	TIMSK |= (1 << OCIE1A);

	sei();
}

// seven segment display
volatile uint8_t ssd1,ssd2,ssd3,ssd4; 

ISR(TIMER1_COMPA_vect)
{
   count++;

   ssd4 = num[count % 10];
   ssd3 = num[(count / 10) % 10];
   ssd2 = num[(count / 100) % 10];
   ssd1 = num[(count / 1000) % 10];
}

inline 
void display_num(uint8_t pin, uint8_t num)
{
	DDRC    = _BV(pin);
	PORTD   = num;

	_delay_ms(2);
}

inline 
void display(uint8_t ssd1, uint8_t ssd2, uint8_t ssd3, uint8_t ssd4)
{
	display_num(PC1,ssd1);
	display_num(PC2,ssd2);
	display_num(PC3,ssd3);
	display_num(PC4,ssd4);
}

void main()
{
	init();

	count = 0;  

    ssd1 = num[0]; 
    ssd2 = num[0];
    ssd3 = num[0];
    ssd4 = num[0];

	for (;;)
		display(ssd1,ssd2,ssd3,ssd4);
}
