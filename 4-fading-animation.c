#include <avr/io.h>
#include <util/delay.h>

int main() {
    DDRC |= _BV(PC0) | _BV(PC1) | _BV(PC2) |
                _BV(PC3) | _BV(PC4) | _BV(PC5);
    PORTC = 0;
    
    volatile int i;
    volatile int j;
    volatile int d;
    for(;;) {
        
        for (d = 0; d != 8; d++) {

            if (d > 0 && d < 7) {
                PORTC |= _BV(d-1);
            } 

            if (d > 1 && d < 8) { 
                for (i = 120; i >= 0; i -= 2) {
                    PORTC |= _BV(d-2);
                    for (int j=0; j < i; j++);
                    PORTC &= ~(_BV(d-2));
                    _delay_ms(3);
                }
            }

            if (d < 6) {
                for (i = 0; i < 120; i += 2) {
                    PORTC |= _BV(d);
                    for (int j=0; j < i; j++);
                    PORTC &= ~(_BV(d));
                    _delay_ms(3);
                }
            }
        }
    }
}
