#include <avr/io.h>
#include <util/delay.h>

#define BUTTON_PORT PORTD
#define BUTTON_PIN PIND
#define BUTTON_DDR DDRD
#define BUTTON_BIT PD0

#define LED_PORT PORTC
#define LED_BIT PC0
#define LED_DDR DDRC

int main() {
    /* led port as output */
    LED_DDR |= _BV(LED_BIT);

    /* button port line as input */
    BUTTON_DDR &= ~(_BV(BUTTON_BIT));

    /* led is OFF initially */
    LED_PORT &= ~(_BV(LED_BIT));    

    /* */
    BUTTON_PORT |= _BV(BUTTON_BIT);
    
    for(;;) {
        if (bit_is_clear(BUTTON_PIN, BUTTON_BIT)) { 
            _delay_ms(25);
            while (bit_is_clear(BUTTON_PIN, BUTTON_BIT));
            LED_PORT ^= _BV(LED_BIT);
            _delay_ms(25);
        }

    }
    return 0;
}
